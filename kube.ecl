{
	environment = env:"CI_COMMIT_REF_SLUG" || "master"
	image = env:"CI_REGISTRY_IMAGE" || "registry.gitlab.com/kevincox/dontsayit-api"
	version = env:"CI_COMMIT_SHA" || "master"
	base-hostname = "api.dontsayit.kevincox.ca"

	env-selector = {
		app-env = "dontsayit-api-{environment}"
	}

	common-metadata = {
		namespace = "dontsayit-api"
		name = "dontsayit-api-{environment}"
		labels = env-selector
	}

	r = {
		apiVersion = "v1"
		kind = "List"
		metadata = {}

		items = [{
			apiVersion = "v1"
			kind = "Namespace"
			metadata.name = common-metadata.namespace
		} {
			apiVersion = "apps/v1"
			kind = "Deployment"
			metadata = common-metadata
			spec = {
				replicas = 1
				selector.matchLabels = env-selector
				template = {
					metadata = common-metadata
					spec.containers = [{
						name = "server"
						image = "{......image}:{version}"
						ports = [{containerPort = 8080}]
					}]
				}
			}
		} {
			apiVersion = "v1"
			kind = "Service"
			metadata = common-metadata
			spec = {
				ports = [{port = 8080}]
				selector = env-selector
			}
		} {
			apiVersion = "networking.k8s.io/v1"
			kind = "Ingress"
			metadata = common-metadata:{
				annotations."cert-manager.io/cluster-issuer" = "letsencrypt"
			}
			spec = {
				tls = [{
					hosts = [
						base-hostname
						"*.{base-hostname}"
					]
					secretName = "tls"
				}]
				rules = [{
					host = cond:[
						environment == "master" base-hostname
						"{version}.{base-hostname}"
					]
					http.paths = [{
						path = "/"
						pathType = "Prefix"
						backend.service = {
							name = common-metadata.name
							port.number = 8080
						}
					}]
				}]
			}
		}]
	}
}.r
