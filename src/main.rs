mod data;
use crate::data::*;

use rand::{thread_rng, Rng};
use rand::seq::SliceRandom as _;
use rustc_serialize::{Encoder as EncoderTrait, Encodable};
use rustc_serialize::json::{Json, Object, Encoder, EncodeResult};
use std::collections::hash_map::{HashMap, Entry};
use std::env;
use std::mem;
use std::sync::{Arc, Weak, Mutex, MutexGuard};
use timer::Timer;

macro_rules! _ref_type {
	(U64 $var:ident) => { $var };
	($t:ident $var:ident) => {
		ref $var
	};
}

macro_rules! json_get {
	($data:expr, $key:expr, $typ:ident) => {
		match $data.get($key) {
			Some(&Json::$typ(_ref_type!($typ v))) => v,
			Some(_) => return Err(format!("Expected {} for {}", stringify!($typ), $key)),
			None => return Err(format!("Missing {}", $key)),
		}
	};
}

const ROUND_LENGTH: std::time::Duration = std::time::Duration::from_secs(60);
const ID_ALPHABET: &'static [u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

struct Games {
	games: Mutex<HashMap<String, Arc<Mutex<Game>>>>,
	shared: Arc<GameShared>,
}

impl Games {
	fn make_game(&self) -> Arc<Mutex<Game>> {
		Arc::new(Mutex::new(Game::new(self.shared.clone())))
	}
	
	fn create_with_id(&self, id: String) -> bool {
		println!("Creating game {}", id);
		let mut games = self.games.lock().unwrap();
		let entry = games.entry(id);
		match entry {
			Entry::Occupied(_) => false,
			Entry::Vacant(e) => {
				e.insert(self.make_game());
				true
			}
		}
	}
	
	fn create(&self) -> String {
		let mut difficulty = 0;
		let mut id = String::with_capacity(8);
		let mut rng = thread_rng();
		
		loop {
			let len = 4 + difficulty/2;
			id.clear();
			for _ in 0..len {
				id.push(*ID_ALPHABET.choose(&mut rng).unwrap() as char);
			}
			
			if self.create_with_id(id.clone()) {
				break
			}
			
			difficulty += 1;
		}
		
		id
	}
	
	// fn find(&self, id: &String) -> Option<Arc<Mutex<Game>>> {
	// 	self.games.borrow().get(id).map(|game| game.clone())
	// }
	
	fn find_or_create(&self, id: String) -> Arc<Mutex<Game>> {
		let game = self.games
			.lock().unwrap()
			.entry(id)
			.or_insert_with(|| self.make_game()).clone();
		
		let weak = Arc::downgrade(&game);
		game.lock().unwrap().this = weak;
		
		game
	}
	
	fn clean(&self) {
		let mut games = self.games.lock().unwrap();
		let cutoff = std::time::Instant::now() - std::time::Duration::from_secs(4 * 3600);
		let mut toremove = Vec::new();
		
		println!("Cleaning");
		
		for (id, game) in &*games {
			let game = game.lock().unwrap();
			
			if game.lastused > cutoff {
				continue
			}
			
			toremove.push(id.clone());
		};
		
		for id in toremove {
			println!("Removing game {}", id);
			games.remove(&id);
		}
	}
}

struct GameShared {
	words: Words,
	timer: Mutex<Timer>,
}

struct Game {
	shared: Arc<GameShared>,
	this: Weak<Mutex<Game>>,
	lastused: std::time::Instant,
	
	teams: Vec<Team>,
	players: Vec<Player>,
	connections: Vec<MessageSender>,
	
	phase: Phase,
	round_end: String,
	current_team: usize,
	current_word: usize,
}

impl Game {
	fn new(shared: Arc<GameShared>) -> Self {
		let current_word = thread_rng().gen_range(0..shared.words.len());
		
		Game {
			shared: shared,
			this: Weak::new(),
			lastused: std::time::Instant::now(),
			
			teams: Vec::new(),
			players: Vec::new(),
			connections: Vec::new(),
			
			phase: Phase::Join,
			round_end: String::new(),
			current_team: 0,
			current_word: current_word,
		}
	}
	
	fn heartbeat(&mut self) {
		self.lastused = std::time::Instant::now();
	}
	
	fn current_team(&self) -> &Team {
		&self.teams[self.current_team]
	}
	
	fn current_player_id(&self) -> usize {
		self.current_team().speaker
	}
	
	fn current_player(&self) -> &Player {
		&self.players[self.current_player_id()]
	}
	
	fn current_word(&self) -> &Word {
		&self.shared.words[self.current_word]
	}
	
	fn new_player(&mut self, secret: u64, team: usize) -> usize {
		let id = self.players.len();
		let name = format!("Player {}", id + 1);
		
		let previous_player = self.teams[team].last_player;
		let next_player;
		if previous_player == usize::max_value() {
			next_player = id;
			self.teams[team].speaker = id;
		} else {
			next_player = self.players[previous_player].next_player;
			self.players[previous_player].next_player = id;
		}
		self.teams[team].last_player = id;
		
		println!("Me: {}, Prev: {}, Next: {}", id, previous_player, next_player);
		
		self.players.push(Player{
			name: name,
			secret: secret,
			team: team,
			score: 0,
			next_player: next_player,
		});
		
		for c in 0..self.connections.len() {
			self.send_player(id, c);
		}
		
		id
	}
	
	fn player_set_name(&mut self, id: usize, name: &str) {
		self.players[id].name.clear();
		self.players[id].name.push_str(name);
		
		for c in 0..self.connections.len() {
			self.send_player(id, c);
		}
	}
	
	fn send_player(&mut self, id: usize, conn: usize) {
		let ref conn = self.connections[conn];
		let ref p = self.players[id];
		conn.send("player", |enc| {
			enc.emit_map_elt_key(1, |e| e.emit_str("id"))?;
			enc.emit_map_elt_val(1, |e| e.emit_usize(id))?;
			enc.emit_map_elt_key(1, |e| e.emit_str("team"))?;
			enc.emit_map_elt_val(1, |e| e.emit_usize(p.team))?;
			enc.emit_map_elt_key(1, |e| e.emit_str("name"))?;
			enc.emit_map_elt_val(1, |e| e.emit_str(&p.name))?;
			enc.emit_map_elt_key(1, |e| e.emit_str("score"))?;
			enc.emit_map_elt_val(1, |e| e.emit_i64(p.score))?;
			Ok(())
		});
	}
	
	fn new_team(&mut self, name: String) -> usize {
		let id = self.teams.len();
		self.teams.push(Team{
			name: name,
			score: 0,
			speaker: usize::max_value(), // Shouldn't be used.
			last_player: usize::max_value(),
		});
		
		for c in 0..self.connections.len() {
			self.send_team(id, c);
		}
		
		id
	}
	
	fn send_team(&mut self, id: usize, conn: usize) {
		let ref conn = self.connections[conn];
		let ref t = self.teams[id];
		conn.send("team", |enc| {
			enc.emit_map_elt_key(1, |e| e.emit_str("id"))?;
			enc.emit_map_elt_val(1, |e| e.emit_usize(id))?;
			enc.emit_map_elt_key(1, |e| e.emit_str("name"))?;
			enc.emit_map_elt_val(1, |e| e.emit_str(&t.name))?;
			enc.emit_map_elt_key(1, |e| e.emit_str("score"))?;
			enc.emit_map_elt_val(1, |e| e.emit_i64(t.score))?;
			Ok(())
		});
	}
	
	fn start(&mut self) -> Result {
		self.heartbeat();
		
		if self.phase != Phase::Join { return Err("Game must be in join phase.".to_string()) }
		
		self.next_round()
	}
	
	fn next_round(&mut self) -> Result {
		self.heartbeat();
		
		assert!(!self.teams.is_empty());
		
		self.current_team = (self.current_team + 1) % self.teams.len();
		
		// Advance speaker.
		let s = self.current_player().next_player;
		self.teams[self.current_team].speaker = s;
		
		self.phase = Phase::Prepare;
		
		for c in 0..self.connections.len() {
			self.send_round(c);
		}
		
		Ok(())
	}
	
	fn send_round(&mut self, conn: usize) {
		let ref conn = self.connections[conn];
		conn.send("round", |enc| {
			enc.emit_map_elt_key(1, |e| e.emit_str("speaker"))?;
			enc.emit_map_elt_val(1, |e| e.emit_usize(self.current_player_id()))?;
			enc.emit_map_elt_key(1, |e| e.emit_str("length"))?;
			enc.emit_map_elt_val(1, |e| e.emit_u64(ROUND_LENGTH.as_secs()))?;
			
			enc.emit_map_elt_key(1, |e| e.emit_str("end"))?;
			if self.phase == Phase::Guess {
				enc.emit_map_elt_val(1, |e| e.emit_str(&self.round_end))?;
			} else {
				enc.emit_map_elt_val(1, |e| e.emit_nil())?;
			}
			
			Ok(())
		});
	}
	
	fn words(&mut self) -> &Words {
		&self.shared.words
	}
	
	fn next_word(&mut self) {
		if self.phase == Phase::Prepare {
			self.phase = Phase::Guess;
			
			let end = chrono::offset::Utc::now() + chrono::Duration::from_std(ROUND_LENGTH).unwrap();
			let this = self.this.clone();
			self.shared.timer.lock().unwrap().schedule_with_date(end, move || {
				let arc = match this.upgrade() {
					Some(arc) => arc,
					None => return,
				};
				let mut this = arc.lock().unwrap();
				this.next_round().unwrap()
			}).ignore();
			self.round_end = end.to_rfc3339();
			
			for c in 0..self.connections.len() {
				self.send_round(c);
			}
		}
		self.current_word = (self.current_word + 1) % self.words().len();
		
		for c in 0..self.connections.len() {
			self.send_word(c);
		}
	}
	
	fn send_word(&mut self, conn: usize) {
		let ref conn = self.connections[conn];
		let ref word = self.current_word();
		let player = match conn.player {
			Some(id) => id,
			None => return, // Don't send to someone not on a team yet.
		};
		
		// If the player this connection is for is on the guessing team and not
		// the speaker don't send them the word.
		if
			self.players[player].team == self.current_team &&
			player != self.current_player_id()
		{
			return
		}
		
		conn.send("word", |enc| {
			enc.emit_map_elt_key(1, |e| e.emit_str("word"))?;
			enc.emit_map_elt_val(1, |e| e.emit_str(word.word))?;
			enc.emit_map_elt_key(1, |e| e.emit_str("banned"))?;
			enc.emit_map_elt_val(1, |e| word.forbidden.encode(e))?;
			
			Ok(())
		});
	}
	
	fn add_connection(&mut self, sender: MessageSender) -> usize {
		self.heartbeat();
		
		let conn = self.connections.len();
		self.connections.push(sender);
		
		for p in 0..self.teams.len() {
			self.send_team(p, conn);
		}
		for p in 0..self.players.len() {
			self.send_player(p, conn);
		}
		
		conn
	}
	
	fn register(&mut self, connid: usize, id: usize) {
		self.heartbeat();
		
		{
			let ref mut conn = self.connections[connid];
			
			conn.player = Some(id);
			conn.send("you", |enc| {
				enc.emit_map_elt_key(1, |e| e.emit_str("player"))?;
				enc.emit_map_elt_val(1, |e| e.emit_usize(id))?;
				Ok(())
			});
		}
		
		if self.phase != Phase::Join {
			self.send_round(connid);
		}
		if self.phase == Phase::Guess {
			self.send_word(connid);
		}
	}
	
	fn word_failed(&mut self, word: &str, banned: &str) {
		if word != self.current_word().word {
			return
		}
		
		self.mark_points(-1);
		
		for conn in &self.connections {
			conn.send("word_done", |enc| {
				enc.emit_map_elt_key(1, |e| e.emit_str("from_player"))?;
				enc.emit_map_elt_val(1, |e| e.emit_usize(self.current_player_id()))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("word"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str(word))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("success"))?;
				enc.emit_map_elt_val(1, |e| e.emit_bool(false))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("points"))?;
				enc.emit_map_elt_val(1, |e| e.emit_i32(-1))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("reason"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str("banned"))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("banned"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str(banned))?;
				Ok(())
			});
		}
		
		self.next_word();
	}
	
	fn word_guessed(&mut self, word: &str) {
		if word != self.current_word().word {
			return
		}
		
		self.mark_points(1);
		
		for conn in &self.connections {
			conn.send("word_done", |enc| {
				enc.emit_map_elt_key(1, |e| e.emit_str("from_player"))?;
				enc.emit_map_elt_val(1, |e| e.emit_usize(self.current_player_id()))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("word"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str(word))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("success"))?;
				enc.emit_map_elt_val(1, |e| e.emit_bool(true))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("points"))?;
				enc.emit_map_elt_val(1, |e| e.emit_i32(1))?;
				Ok(())
			});
		}
		
		self.next_word();
	}
	
	fn word_skipped(&mut self, word: &str) {
		if word != self.current_word().word {
			return
		}
		
		self.mark_points(-1);
		
		for conn in &self.connections {
			conn.send("word_done", |enc| {
				enc.emit_map_elt_key(1, |e| e.emit_str("from_player"))?;
				enc.emit_map_elt_val(1, |e| e.emit_usize(self.current_player_id()))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("word"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str(word))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("success"))?;
				enc.emit_map_elt_val(1, |e| e.emit_bool(false))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("points"))?;
				enc.emit_map_elt_val(1, |e| e.emit_i32(-1))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("reason"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str("skipped"))?;
				Ok(())
			});
		}
		
		self.next_word();
	}
	
	fn word_unknown(&mut self, word: &str) {
		if word != self.current_word().word {
			return
		}
		
		for conn in &self.connections {
			conn.send("word_done", |enc| {
				enc.emit_map_elt_key(1, |e| e.emit_str("from_player"))?;
				enc.emit_map_elt_val(1, |e| e.emit_usize(self.current_player_id()))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("word"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str(word))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("success"))?;
				enc.emit_map_elt_val(1, |e| e.emit_nil())?;
				enc.emit_map_elt_key(1, |e| e.emit_str("points"))?;
				enc.emit_map_elt_val(1, |e| e.emit_i32(0))?;
				enc.emit_map_elt_key(1, |e| e.emit_str("reason"))?;
				enc.emit_map_elt_val(1, |e| e.emit_str("unknown"))?;
				Ok(())
			});
		}
		
		self.next_word();
	}
	
	fn mark_points(&mut self, points: i64) {
		let id = self.current_team;
		self.teams[id].score += points;
		
		for c in 0..self.connections.len() {
			self.send_team(id, c);
		}
	}
}

#[derive(PartialEq)]
enum Phase {
	Join, Prepare, Guess
}

struct Team {
	name: String,
	score: i64,
	
	last_player: usize,
	speaker: usize,
}

#[derive(Debug)]
struct Player {
	name: String,
	secret: u64,
	team: usize,
	score: i64,
	next_player: usize,
}

pub struct MessageSender {
	player: Option<usize>,
	ws: ws::Sender,
}

impl MessageSender {
	fn send<F>(&self, kind: &str, write: F)
		where F: FnOnce(&mut Encoder) -> EncodeResult<()> {
		let mut r = String::new();
		
		{
			let mut encoder = Encoder::new(&mut r);
			// Pass a size of 1 because in JSON it doesn't matter (except for 0).
			encoder.emit_map(1, |mut encoder| {
				encoder.emit_map_elt_key(0, |e| e.emit_str("type"))?;
				encoder.emit_map_elt_val(0, |e| e.emit_str(kind))?;
				
				write(&mut encoder)
			}).expect("JSON encoding failed.");
		}
		
		match self.ws.send(ws::Message::Text(r)) {
			Ok(()) => {},
			Err(err) => {
				println!("Error sending: {:?}", err);
				let _ = self.ws.close(ws::CloseCode::Abnormal);
			}
		}
	}
	
	fn _send_log(&mut self, level: &str, msg: &str) {
		println!("LOG: {}: {}", level, msg);
		self.send("log", |encoder| {
			encoder.emit_map_elt_key(1, |e| e.emit_str("level"))?;
			encoder.emit_map_elt_val(1, |e| e.emit_str(level))?;
			encoder.emit_map_elt_key(1, |e| e.emit_str("msg"))?;
			encoder.emit_map_elt_val(1, |e| e.emit_str(msg))?;
			Ok(())
		});
	}
	
	pub fn log_info(&mut self, msg: &str) {
		self._send_log("info", msg)
	}
	
	pub fn log_warn(&mut self, msg: &str) {
		self._send_log("warn", msg)
	}
	
	fn log_error(&mut self, msg: &str) {
		self._send_log("error", msg)
	}
	
}

struct MessageReciever<'s> {
	games: &'s Games,
	sender: Option<MessageSender>,
	game: Option<Arc<Mutex<Game>>>,
	connection: Option<usize>,
}

type Result = std::result::Result<(), String>;

impl<'a> MessageReciever<'a> {
	fn game(&mut self) -> MutexGuard<Game> {
		self.game.as_ref().expect("Game borrowed.").lock().expect("Game poisoned.")
	}
	
	fn sender<F,R>(&mut self, cb: F) -> R
		where F: FnOnce(&mut MessageSender) -> R
	{
		if let Some(sender) = self.sender.as_mut() {
			return cb(sender)
		}
		
		let conn = self.connection.unwrap();
		cb(&mut self.game().connections[conn])
	}
	
	fn wrapped_on_message(&mut self, msg: &ws::Message) -> Result {
		println!("Got message: {}", msg);
		let msg = match *msg {
			ws::Message::Text(ref s) => s,
			ws::Message::Binary(_) => {
				return Err("Binary message recieved.".to_string());
			},
		};
		
		match Json::from_str(&msg) {
			Err(_) => Err("Invalid JSON recieved.".to_string()),
			Ok(Json::Object(data)) => self.dispatch(&data),
			_ => Err("Data was not a JSON object.".to_string()),
		}
	}
	
	fn dispatch(&mut self, data: &Object) -> Result {
		match data.get("type") {
			None => Err("Message didn't have a type".to_string()),
			Some(&Json::String(ref s)) => {
				match s.as_ref() {
					"register" => {
						let team = data.get("team").ok_or("Missing team")?;
						let secret = json_get!(data, "secret", U64);
						
						let connection = self.connection.unwrap();
						
						let mut game = self.game();
						let team = match team {
							&Json::U64(id) => {
								let id = id as usize;
								if id >= game.teams.len() {
									return Err("Team doesn't exist.".to_string())
								}
								id
							}
							&Json::String(ref name) => {
								let name = name.trim();
								if name.is_empty() {
									return Err("Team name can't be empty.".to_string())
								}
								game.new_team(name.to_string())
							},
							_ => return Err("Unsupported type for team.".to_string()),
						};
						
						let id = game.new_player(secret, team);
						game.register(connection, id);
						
						Ok(())
					},
					"rejoin" => {
						let id = json_get!(data, "id", U64) as usize;
						let secret = json_get!(data, "secret", U64);
						
						let connection = self.connection.unwrap();
						
						let mut game = self.game();
						{
							let player = match game.players.get(id) {
								Some(p) => p,
								None => return Err("Player does not exist.".to_string()),
							};
							
							if player.secret != secret {
								return Err("Wrong secret.".to_string());
							}
						}
						
						game.register(connection, id);
						
						Ok(())
					},
					"player" => {
						let connection = self.connection.unwrap();
						
						let id = json_get!(data, "id", U64) as usize;
						let name = json_get!(data, "name", String);
						
						let mut game = self.game();
						if id >= game.players.len() {
							return Err("Player does not exist.".to_string())
						};
						
						if Some(id) != game.connections[connection].player {
							return Err("You can only update yourself.".to_string())
						}
						
						game.player_set_name(id, name);
						
						Ok(())
					},
					"start" => {
						if let None = self.sender(|s| s.player) {
							return Err("You must be registered to start a game.".to_string());
						}
						
						self.game().start()
					},
					"round_start" => {
						self.game().next_word();
						
						Ok(())
					},
					"word_failed" => {
						let word = json_get!(data, "word", String);
						let banned = json_get!(data, "banned", String);
						
						self.game().word_failed(word, banned);
						
						Ok(())
					}
					"word_guessed" => {
						let word = json_get!(data, "word", String);
						
						let connection = self.connection.unwrap();
						
						let mut game = self.game();
						let us = game.connections[connection].player;
						if Some(game.current_player_id()) != us {
							return Err("Only the speaker can mark a word guessed.".to_string());
						}
						
						game.word_guessed(word);
						
						Ok(())
					}
					"word_skip" => {
						let word = json_get!(data, "word", String);
						
						let connection = self.connection.unwrap();
						
						let mut game = self.game();
						let us = game.connections[connection].player;
						if Some(game.current_player_id()) != us {
							return Err("Only the speaker can mark a word skipped.".to_string());
						}
						
						game.word_skipped(word);
						
						Ok(())
					}
					"word_unknown" => {
						let word = json_get!(data, "word", String);
						
						let connection = self.connection.unwrap();
						
						let mut game = self.game();
						let us = game.connections[connection].player;
						if Some(game.current_player_id()) != us {
							return Err("Only the speaker can mark a word unknown.".to_string());
						}
						
						game.word_unknown(word);
						
						Ok(())
					}
					_ => Err(format!("Unkown message type {:?}", &s)),
				}
			},
			Some(_) => Err("Message type wasn't a string.".to_string()),
		}
	}
}

impl<'a> ws::Handler for MessageReciever<'a> {
	fn on_request(&mut self, req: &ws::Request) -> ws::Result<ws::Response> {
		let path = req.resource().split("/").collect::<Vec<&str>>();
		assert!(path[0] == "");
		match path[1] {
			"create" => {
				let id = self.games.create();
				let response = format!("\
					HTTP/1.0 201 CREATED\r\n\
					Access-Control-Allow-Origin: *\r\n\
					Access-Control-Allow-Methods: GET, POST, PUT, DELETE\r\n\
					Access-Control-Allow-Headers: Accept, Authorization, Content-Type\r\n\
					Access-Control-Allow-Max-Age: 31536000\r\n\
					Access-Control-Expose-Headers: DSI-ID\r\n\
					DSI-ID: {}\r\n\
					\r\n", id);
				Ok(ws::Response::parse(response.as_bytes())
					.unwrap().unwrap())
			},
			"game" => {
				let mut res = ws::Response::from_request(req)?;
				res.set_protocol("ca.kevincox.dontsayit.v1");
				
				let id = path[2].to_string();
				self.game = Some(self.games.find_or_create(id));
				let sender = self.sender.take().unwrap();
				let connection = self.game().add_connection(sender);
				self.connection = Some(connection);
				
				Ok(res)
			},
			"ping" => {
				Ok(ws::Response::new(200, "OK", "2a3fcf63-7ec5-4d03-9b7f-3abb02f8ecff".into()))
			},
			_ => {
				Ok(ws::Response::new(404, "NOT FOUND", "".into()))
			},
		}
	}
	
	fn on_open(&mut self, _: ws::Handshake) -> ws::Result<()> {
		Ok(())
	}
	
	fn on_message(&mut self, msg: ws::Message) -> ws::Result<()> {
		if let Err(desc) = self.wrapped_on_message(&msg) {
			self.sender(|s| s.log_error(&desc));
			println!("{}", desc);
		}
		
		Ok(())
	}
}

fn env(k: &str, default: &str) -> String {
	match env::var(k) {
		Ok(s) => s,
		Err(env::VarError::NotPresent) => default.into(),
		Err(env::VarError::NotUnicode(_)) => {
			panic!("The environemnt variable {:?} isn't valid UTF8", k)
		},
	}
}

fn main() {
	env_logger::init();
	
	let bind = env("DSI_BIND", "127.0.0.1:8080");
	let wordfile = env("DSI_WORDS", "data/words.csv");
	
	
	let shared = GameShared{
		words: Words::from_path(&wordfile).expect("Couldn't read word file."),
		timer: Mutex::new(Timer::new()),
	};
	let games = Games{
		games: Mutex::new(HashMap::new()),
		shared: Arc::new(shared),
	};
	let _cleanup = {
		let games: &'static Games = unsafe { mem::transmute(&games) };
		games.shared.timer.lock().unwrap().schedule_repeating(chrono::Duration::hours(1), move || {
			games.clean();
		})
	};
	let ws = ws::Builder::new()
		.build(|sender| MessageReciever{
			sender: Some(MessageSender{ws: sender, player: None}),
			games: &games,
			game: None,
			connection: None,
		}).unwrap();

	println!("Listening at http://{}", bind);
	ws.listen(bind.as_str()).unwrap();
}
