extern crate murmur3;

use std::fmt::{Debug, Formatter, Result as FmtResult};
use std::fs::File;
use std::io::{Read, Result};
use std::mem;
use std::ops;
use std::path::Path;

pub struct Words {
	words: Vec<Word>,
	data: String,
}

// Note that lifetimes of references are not actually static.
pub struct Word {
	pub word: &'static str,
	pub forbidden: Vec<&'static str>,
}

impl Words {
	pub fn from_file(data: &mut impl Read) -> Result<Words> {
		let mut buf = String::new();
		data.read_to_string(&mut buf)?;
		Self::from_string(buf)
	}
	
	pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Words> {
		Self::from_file(&mut File::open(path)?)
	}
	
	pub fn from_string(data: String) -> Result<Words> {
		let mut r = Words{words: Vec::new(), data: data};
		
		// The string will live as long as the parent struct so as far as we
		// are concerned it is essentially static.
		let data: &'static str = unsafe { mem::transmute(&*r.data) };
		
		// The buffer is managed in the parent structure so it won't be
		// deallocated until we are.
		for l in data.lines() {
			let l = l.trim();
			if l.starts_with("#") || l.is_empty() {
				continue
			}
			
			let mut iter = l.split(",").map(|w| w.trim());
			let word = iter.next().expect("Line didn't have any words.");
			let forbidden: Vec<_> = iter.collect();
			
			r.words.push(Word{word: word, forbidden: forbidden})
		}
		
		r.words.sort_by_key(|s| {
			murmur3::murmur3_32(&mut s.word.as_bytes(), 0).unwrap()
		});
		
		Ok(r)
	}
}

impl ops::Deref for Words {
	type Target = Vec<Word>;
	
	fn deref(&self) -> &Self::Target {
		&self.words
	}
}

impl Debug for Words {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f, "Words[\n")?;
		for w in &self.words {
			write!(f, "\t{:?},\n", w)?;
		}
		write!(f, "]")
	}
}

impl Debug for Word {
	fn fmt(&self, f: &mut Formatter) -> FmtResult {
		write!(f, "Word{{w: {:?}, banned: {:?}}}", self.word, self.forbidden)
	}
}
