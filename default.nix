{
	nixpkgs ? import <nixpkgs> {},
	ecl ? builtins.storePath (nixpkgs.lib.fileContents (builtins.fetchurl "https://kevincox.gitlab.io/ecl/ecl.nixpath")),
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nmattia/naersk/archive/master.tar.gz") {},
}:
with nixpkgs;
rec {
	out = naersk.buildPackage {
		root = pkgs.nix-gitignore.gitignoreSource [
			"*.ecl"
			"*.nix"
			".*"
		] ./.;
		buildInputs = with pkgs; [
			openssl
			pkg-config
		];
		doCheck = true;
	};
	
	docker = dockerTools.buildImage {
		name = "dontsayit-api";
		tag = "latest";
		config = {
			Cmd = [ "${out}/bin/dontsayit-api" ];
			Env = [
				"RUST_BACKTRACE=t"
				"DSI_BIND=0.0.0.0:8080"
				"DSI_WORDS=${./data/words.csv}"
			];
			ExposedPorts = { "8080/tcp" = {}; };
		};
	};

	kube = pkgs.runCommand "kube.json" {
		CI_COMMIT_REF_SLUG = lib.maybeEnv "CI_COMMIT_REF_SLUG" null;
		CI_COMMIT_SHA = lib.maybeEnv "CI_COMMIT_SHA" null;
		CI_REGISTRY_IMAGE = lib.maybeEnv "CI_REGISTRY_IMAGE" null;
	} ''
		${ecl}/bin/ecl load ${./kube.ecl} >$out
		cat $out
	'';
}
